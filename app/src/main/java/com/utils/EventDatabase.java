package com.utils;

/**
 * Created by serdarbuyukkanli on 15/06/15.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.metrics34.tracking.TrackingEvent;
import com.metrics34.tracking.ibeacon.IBeacon;

/**
 * Helper for the event logging database.
 */
public class EventDatabase {
    /**
     * Open database helper to initialize the schema.
     */
    private static class MyDatabaseHelper extends SQLiteOpenHelper {
        /**
         * Create a new database for a given context.
         * @param context The context.
         */
        public MyDatabaseHelper(Context context) {
            super(context, "events", null, 1);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE events(" +
                    "_id INTEGER PRIMARY KEY," +
                    "timestamp INTEGER," +
                    "eventtype INTEGER," +
                    "proximityUUID TEXT," +
                    "proximity INTEGER," +
                    "major INTEGER," +
                    "minor INTEGER," +
                    "bluetooth TEXT," +
                    "rssi INTEGER," +
                    "txpower INTEGER," +
                    "accuracy REAL," +
                    "url TEXT" +
                    ");");
            db.execSQL("CREATE INDEX events_by_time ON events ( timestamp DESC );");
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    /**
     * Provate singleton sqlitedatabase instance.
     */
    private static SQLiteDatabase db = null;

    /**
     * Columns on events table, used as projection.
     */
    private final static String COLUMNS[] = new String[]{
            "_id", "timestamp", "eventtype", "proximityUUID", "proximity",
            "major", "minor", "bluetooth",
            "rssi", "txpower", "accuracy", "url"
    };

    /**
     * Type of uploaded events (non-mock).
     */
    public static int TYPE_UPLOAD = 0;

    /**
     * Type of kept (non-uploaded) events.
     */
    public static int TYPE_KEPT = 1;

    /**
     * Uri for this DB, used to notify the cursor on updates.
     */
    private final static Uri URI = Uri.parse("content://" + EventDatabase.class.getCanonicalName() + "/events");

    /**
     * Log an event.
     * @param context The context.
     * @param beacon The beacon.
     * @param event The tracking event.
     * @param type The type (TYPE_UPLOAD or TYPE_KEPT).
     */
    public static synchronized void log(Context context, IBeacon beacon, TrackingEvent event, int type) {
        if (db == null) {
            db = new MyDatabaseHelper(context).getWritableDatabase();
        }
        ContentValues values = new ContentValues();
        values.put("timestamp", System.currentTimeMillis());
        values.put("eventtype", type);
        values.put("proximityUUID", beacon.getProximityUuid());
        values.put("proximity", beacon.getProximity());
        values.put("major", beacon.getMajor());
        values.put("minor", beacon.getMinor());
        values.put("bluetooth", beacon.getBluetoothAddress());
        values.put("accuracy", beacon.getAccuracy());
        values.put("rssi", beacon.getRssi());
        values.put("txpower", beacon.getTxPower());
        try {
            values.put("url", event.getTargetURL(false));
        } catch (IllegalStateException e) {
            // if tracking setup is broken.
            e.printStackTrace();
        }
        db.insert("events", null, values);
        context.getContentResolver().notifyChange(URI, null);
    }

    /**
     * Query this DB for the latest events.
     * @param context The context.
     * @return A new cursor.
     */
    public static synchronized Cursor query(Context context) {
        if (db == null) {
            db = new MyDatabaseHelper(context).getWritableDatabase();
        }
        Cursor c = db.query("events", COLUMNS, null, null, null, null, "timestamp DESC", "10");
        c.setNotificationUri(context.getContentResolver(), URI);
        return c;
    }

}
