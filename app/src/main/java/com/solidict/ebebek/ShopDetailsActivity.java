package com.solidict.ebebek;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ShopDetailsActivity extends Activity {

	private TextView nameText;
	private TextView addressText;
	private TextView telephoneText;
	private TextView workingHoursText;
	private ImageView imageView;
	private Button mapButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shop_details_activity);
	
		Bundle extras = getIntent().getExtras();
		String shopDetailsStr = extras.getString("shopDetail");
		final String[] shopDetails = shopDetailsStr.split("#");
		
		nameText = (TextView) findViewById(R.id.textView_name);
		addressText = (TextView) findViewById(R.id.textView_address);
		telephoneText = (TextView) findViewById(R.id.textView_telephone);
		workingHoursText = (TextView) findViewById(R.id.textView_workingHours);
		imageView = (ImageView) findViewById(R.id.imageView);
		mapButton = (Button) findViewById(R.id.button_map);

		nameText.setText(shopDetails[1]);
		addressText.setText(shopDetails[2]);
		telephoneText.setText(shopDetails[3]);
		workingHoursText.setText(shopDetails[4]);
		
		mapButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ShopDetailsActivity.this, AddressShowActivity.class);
				i.putExtra("lat", Double.valueOf(shopDetails[5]));
				i.putExtra("lon", Double.valueOf(shopDetails[6]));
				i.putExtra("title", shopDetails[1]);
				ShopDetailsActivity.this.startActivity(i);
			}
		});
		
		new AsyncTaskWithDialog<Void, Bitmap>(this) {

			@Override
			protected Bitmap doInBackground(Void... params) {
				return ShopsListAdapter.getBmpOfThisImage("http://kiwi.e-bebek.com/i/magaza_resimleri/"+ shopDetails[0]+".jpg");
			}
			
			protected void onPostExecute(Bitmap result) {
				super.onPostExecute(result);
				if(result != null){					
					imageView.setImageBitmap(result);
				}
			};
		}.execute((Void) null);
	}
}
