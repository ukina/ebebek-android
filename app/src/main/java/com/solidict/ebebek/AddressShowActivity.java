package com.solidict.ebebek;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class AddressShowActivity extends MapActivity {

	private MapView mapView;
	private MapController mc;

	private double lat;
	private double lng;
	private String branchName;

	private TextView branchNameTV;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.address_show_activity);

			mapView = (MapView) findViewById(R.id.mapview1);
			branchNameTV = (TextView) findViewById(R.id.textView_sube);

			mc = mapView.getController();

			Bundle extras = getIntent().getExtras();
			lat = extras.getDouble("lat");
			lng = extras.getDouble("lon");
			branchName = extras.getString("title");
			branchNameTV.setText(branchName);

			GeoPoint p = new GeoPoint((int) (lat * 1E6), (int) (lng * 1E6));

			mc.animateTo(p);
			mc.setZoom(17);

			Drawable pinImage = getResources().getDrawable(R.drawable.map_pin);
			// Drawable pinImage = writeOnDrawable(R.drawable.map_pin, "ali veli deli");
			MapPinItemizedOverlay pinOverlay = new MapPinItemizedOverlay(pinImage, this);

			OverlayItem overlayitem = new OverlayItem(p, "", "");
			pinOverlay.setPin(overlayitem);

			mapView.getOverlays().add(pinOverlay);
			mapView.invalidate();

		} catch (Exception e) {
			Log.e("error", e.getMessage(), e);
		}
	}

	public BitmapDrawable writeOnDrawable(int drawableId, String text) {

		Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

		Paint paint = new Paint();
		paint.setStyle(Style.FILL);
		paint.setColor(Color.BLACK);
		paint.setTextSize(20);

		Canvas canvas = new Canvas(bm);
		canvas.drawText(text, 0, bm.getHeight() / 2, paint);

		return new BitmapDrawable(bm);
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
