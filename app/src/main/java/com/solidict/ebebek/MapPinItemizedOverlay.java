package com.solidict.ebebek;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;


public class MapPinItemizedOverlay extends ItemizedOverlay<OverlayItem> {


	private OverlayItem pin;
	private Context context;
	public MapPinItemizedOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
		
	}
	
	public MapPinItemizedOverlay(Drawable defaultMarker, Context context) {
		super(boundCenterBottom(defaultMarker));
		this.context = context;
		
	}

	@Override
	protected OverlayItem createItem(int i) {
		return pin;
	}

	@Override
	public int size() {
		return 1;
	}
	
	@Override
	protected boolean onTap(int index) {
//	  OverlayItem item = pin;
//	  AlertDialog.Builder dialog = new AlertDialog.Builder(context);
//	  dialog.setTitle(item.getTitle());
//	  dialog.setMessage(item.getSnippet());
//	  dialog.show();
	  return true;
	}
	
	public void setPin(OverlayItem overlay) {
	   pin = overlay;
	   populate();
	}

}
