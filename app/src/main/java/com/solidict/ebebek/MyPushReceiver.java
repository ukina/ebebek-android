package com.solidict.ebebek;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Notification.Action;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.client.android.CaptureActivity;
import com.infobip.push.AbstractPushReceiver;
import com.infobip.push.PushNotification;

public class MyPushReceiver extends AbstractPushReceiver {

    @Override
    public void onRegistered(Context context) {
        //Toast.makeText(context, "Successfully registered.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRegistrationRefreshed(Context context) {
        //Toast.makeText(context, "Registration is refreshed.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNotificationReceived(PushNotification notification, Context context) {
//        Toast.makeText(context, "Received notification: " + notification.toString(), 
//             Toast.LENGTH_SHORT).show();
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    	try {
			JSONObject json = new JSONObject(notification.getAdditionalInfo());
			String url = json.getString("url");
			Intent intent = new Intent(context, MainActivity.class);
			intent.putExtra("PushUrl",url);
			intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }

    @Override
    protected void onNotificationOpened(PushNotification notification, Context context) {
//        Toast.makeText(context, "Notification opened.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUnregistered(Context context) {
//        Toast.makeText(context, "Successfully unregistered.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(int reason, Context context) {
//        Toast.makeText(context, "Error occurred: " + reason, Toast.LENGTH_SHORT).show();
    }
}