package com.solidict.ebebek;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class AsyncTaskWithDialog<Params, Result> extends AsyncTask<Params, Long, Result> {

	private ProgressDialog dialog = null;
	private final Context contextForDialog;
	
	public AsyncTaskWithDialog(Context contextForDialog) {
		this.contextForDialog = contextForDialog;
	}
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog = ProgressDialog.show(contextForDialog, "", "Lütfen Bekleyin", true);
	}
	
	protected void onPostExecute(Result result) {
		
		if(dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
		
	};
	

}
