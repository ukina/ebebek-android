package com.solidict.ebebek;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ShopsListActivity extends Activity {

	private String shops[];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.shops_list_activity);
		final ListView lv = (ListView) findViewById(R.id.listView);
		final ShopsListAdapter adapter = new ShopsListAdapter(ShopsListActivity.this);
	
		new AsyncTaskWithDialog<Void, String>(ShopsListActivity.this) {

			@Override
			protected String doInBackground(Void... params) {
				String response = null;
				try {
					URL connectURL = new URL("http://www.e-bebek.com/magazalarimiz_mobile.aspx");
					HttpURLConnection httpURLConnection = (HttpURLConnection) connectURL.openConnection();
					httpURLConnection.setRequestMethod("GET");
					httpURLConnection.setDoOutput(true);
					httpURLConnection.connect();
					int responseCode = httpURLConnection.getResponseCode();

					if (responseCode == HttpURLConnection.HTTP_OK) {
						InputStream is = httpURLConnection.getInputStream();

						byte[] data;
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						int read = is.read();

						while (read != -1) {
							baos.write(read);
							read = is.read();
						}

						baos.flush();
						data = baos.toByteArray();
						response = new String(data);
					}

					return response;
				} catch (Exception e) {
					return null;
				}
			}

			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result == null) {
					AlertDialog.Builder alert = new AlertDialog.Builder(ShopsListActivity.this);
					alert.setTitle("Hata");
					alert.setMessage("Bağlantı Hatası");
					alert.show();
					return;
				}
				shops = result.split(";");
				adapter.setShops(shops);
				lv.setAdapter(adapter);
				lv.invalidateViews();

			};
		}.execute((Void) null);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent i = new Intent (ShopsListActivity.this, ShopDetailsActivity.class);
				i.putExtra("shopDetail", shops[arg2]);
				ShopsListActivity.this.startActivity(i);
			}
		});
	}
}
