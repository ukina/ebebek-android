package com.solidict.ebebek;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ShopsListAdapter implements ListAdapter {
	
	private Context context;
	
	private String[] shops;

	public String[] getShops() {
		return shops;
	}

	public void setShops(String[] shops) {
		this.shops = shops;
	}

	public ShopsListAdapter(Context context) {
		this.context = context;
	}

	public int getCount() {
		return shops.length;
	}

	public Object getItem(int arg0) {
		return 0;
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public int getItemViewType(int arg0) {
		return 0;
	}

	public View getView(int arg0, View arg1, ViewGroup arg2) {
		
		String[] shopDetails = shops[arg0].split("#");

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final LinearLayout view = (LinearLayout) inflater.inflate(R.layout.shop_item, null, false);

		try {
			String imageUrl = "http://kiwi.e-bebek.com/i/magaza_resimleri/"+ shopDetails[0]+".jpg";
			String shopTitle = shopDetails[1];
			String address = shopDetails[2];

			final ImageView thumbnail = (ImageView) view.findViewById(R.id.imageView);
			final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
			TextView shopNameText = (TextView) view.findViewById(R.id.textView_shopName);
			TextView shopAddressText = (TextView) view.findViewById(R.id.textView_shopAddress);
			LinearLayout llforProgress = (LinearLayout) view.findViewById(R.id.linearLayout6);
	
			shopNameText.setText(shopTitle);
			shopAddressText.setText(address);
	
			fetchAndSetImage(imageUrl, llforProgress, thumbnail, progressBar);

			// view.setBackgroundResource(android.R.drawable.list_selector_background);
		} catch (Exception e) {
			Log.e("error", e.getMessage(), e);
		}
		return view;
	}

	private static void fetchAndSetImage( final String imageUrl, final LinearLayout view, final ImageView thumbnail,
			final ProgressBar progressBar) {


			new AsyncTask<String, Long, Bitmap>() {

				protected void onPreExecute() {
					super.onPreExecute();
					thumbnail.setVisibility(ImageView.GONE);

				};

				@Override
				protected Bitmap doInBackground(String... params) {
					return getBmpOfThisImage(params[0]);
				}

				@Override
				protected void onPostExecute(Bitmap result) {
					super.onPostExecute(result);
					thumbnail.setImageBitmap(result);
					view.removeView(progressBar);
					thumbnail.setVisibility(ImageView.VISIBLE);

				}
			}.execute(imageUrl);
		

	}
	
	public static Bitmap getBmpOfThisImage(String url) {
		byte[] bitmapBytes = downloadFromUrl(url);
		Bitmap bmp = null;
		if (bitmapBytes != null) {
			bmp = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length);
		}
		return bmp;
	}

	private static byte[] downloadFromUrl(String imageURL) {

		try {
			URL url = new URL(imageURL);

			URLConnection ucon = url.openConnection();

			InputStream is = ucon.getInputStream();
			BufferedInputStream bis = new BufferedInputStream(is);

			ByteArrayBuffer baf = new ByteArrayBuffer(100);
			int current = 0;
			while ((current = bis.read()) != -1) {
				baf.append((byte) current);
			}

			return baf.toByteArray();

		} catch (IOException e) {
			return null;
		}

	}

	public int getViewTypeCount() {
		return 1;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isEmpty() {
		return (shops!=null && shops.length >0);
	}

	public void registerDataSetObserver(DataSetObserver arg0) {

	}

	public void unregisterDataSetObserver(DataSetObserver arg0) {

	}

	public boolean areAllItemsEnabled() {
		return true;
	}

	public boolean isEnabled(int position) {
		return true;
	}

}
