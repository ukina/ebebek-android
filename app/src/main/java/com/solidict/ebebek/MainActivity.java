package com.solidict.ebebek;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.beaconinside.androidsdk.BeaconService;
import com.google.zxing.client.android.CaptureActivity;
import com.metrics34.tracking.AndroidTrackingEngine;
import com.metrics34.tracking.Beacon;
import com.metrics34.tracking.Click;
import com.metrics34.tracking.TrackingEvent;
import com.metrics34.tracking.ibeacon.tracker.LocalBeaconBroadcastHandler;


@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener{

    //	private static final String baseUrl = "http://mobileapp.ebebek.com";
    private static final String baseUrl = "http://mobileapp2.ebebek.com";
    //	private static final String baseUrl = "http://preprod.m.ebebek.com";
    public static String urlToOpen = null;
    private WebView webView = null;

    protected static String TAG = "demo.BeaconApplication";
    LocalBeaconBroadcastHandler localBeaconBroadcastHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        beacon();


        Bundle extras = getIntent().getExtras();
        String barcode = null;
        String pushUrl = null;
        if (extras != null) {
            barcode = extras.getString("barcodeNum");
            pushUrl = extras.getString("PushUrl");
        }
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        String userAgentString = webView.getSettings().getUserAgentString();
        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 4.2.2; tr-tr; GT-I9295 Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setDomStorageEnabled(true);
        System.out.println("user agent String : " + userAgentString);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });
        if (barcode != null) {
            webView.loadUrl(baseUrl + "/SolrSearch/ListProductByBarcode/" + barcode);
        } else {
            webView.loadUrl(baseUrl);

        }
        if (pushUrl != null) {
            webView.loadUrl(pushUrl);

        }

    }

    private void beacon() {
        String webViewUrl = "http://ctrack.metrics34.com/index.php?redirect=http%3A%2F%2Fmobil.e-bebek.com&cl=1343434323236323131303&bm=100&bmcl=4313735313236323131303&cp=101&ag=101&crid=101&subid=Keyword1";
        TrackingEvent.defaults.engine.initializeWebAppLink(webViewUrl);
        TrackingEvent.defaults.setClient("1343434323236323131303");
        TrackingEvent.defaults.setAccount("8353835313236323131303");
        TrackingEvent.defaults.engine = new
                AndroidTrackingEngine(getApplicationContext());
        TrackingEvent.defaults.engine.init();


        localBeaconBroadcastHandler = LocalBeaconBroadcastHandler.getInstance(getApplicationContext());

        BeaconService.init(this, "49F9aqmhLSxLCTnxZcn3");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuItem tumFirsatlarOptionsItem = menu.add(0, 1, 1, "Anasayfa");
        tumFirsatlarOptionsItem.setIcon(getResources().getDrawable(R.drawable.anasayfaicon));

        MenuItem firsatlarimOptionsItem = menu.add(0, 2, 2, "Ürünler");
        firsatlarimOptionsItem.setIcon(getResources().getDrawable(R.drawable.urunlericon));

        MenuItem firsatAraOptionsItem = menu.add(0, 3, 3, "Sepetim");
        firsatAraOptionsItem.setIcon(getResources().getDrawable(R.drawable.basketicon));

        MenuItem barkodOptionsItem = menu.add(0, 4, 4, "Barkod");
        barkodOptionsItem.setIcon(getResources().getDrawable(R.drawable.barcodeicon));

        MenuItem avmOptionsItem = menu.add(0, 5, 5, "Bana Özel");
        avmOptionsItem.setIcon(getResources().getDrawable(R.drawable.formeicon));

        MenuItem ayarlarOptionsItem = menu.add(0, 6, 6, "Mağazalar");
        ayarlarOptionsItem.setIcon(getResources().getDrawable(R.drawable.shopsicon));

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case 1:
                webView.loadUrl(baseUrl);
                break;
            case 2:
                webView.loadUrl(baseUrl + "/Home/ListMainCategories");
                break;
            case 3:
                webView.loadUrl(baseUrl + "/Basket/Basket");
                break;
            case 4:
                intent = new Intent(this, CaptureActivity.class);
                startActivity(intent);
                break;
            case 5:
                webView.loadUrl(baseUrl + "/Account/Profile");

                break;
            case 6:
                intent = new Intent(this, ShopsListActivity.class);
                startActivity(intent);
                break;

        }

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            webView.goBack();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }

    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

}
